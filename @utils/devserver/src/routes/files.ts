/*
 * This file is part of AudioPlayer (https://gitlab.com/vitoyucepi/audio-player)
 * Copyright (C) 2023 Vitoyucepi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import {Response, Router as router} from 'express';
import {resolve} from 'path';

export const data = router();

data.get('/state.txt', (_, res) => {
  res.sendFile(resolve('data/state.txt'));
});

function setHeaders(res: Response, headers: Record<string, string | number | string[]>) {
  Object.entries(headers).forEach(([header, value]) => {
    res.setHeader(header, value);
  });
}

function configureRadio(metaData: boolean): Record<string, string | number | string[]> {
  const icyBR = 192;
  const icyMetaint = 8192;
  const headers: Record<string, string | number | string[]> = {};
  headers['content-type'] = 'audio/mpeg';
  headers['icy-br'] = icyBR;
  headers['ice-audio-info'] = 'channels=2;samplerate=44100;bitrate=192';
  headers['icy-genre'] = 'radio';
  headers['icy-name'] = 'Radio Anonymous';
  // noinspection HttpUrlsUsage
  headers['icy-url'] = 'http://anon.fm';
  if (metaData) {
    headers['server'] = 'Icecast 2.3.3-kh3';
    headers['icy-metaint'] = icyMetaint;
  }
  return headers;
}

function configureRadioOgg(): Record<string, string | number | string[]> {
  const icyBR = 112;
  const headers: Record<string, string | number | string[]> = {};
  headers['content-type'] = 'application/ogg';
  headers['icy-br'] = icyBR;
  headers['ice-audio-info'] = 'channels=2;samplerate=44100;quality=3%2e00';
  headers['icy-description'] = 'Your ads may be here';
  headers['icy-genre'] = 'radio';
  headers['icy-name'] = 'Radio Anonymous (ogg high quality)';
  return headers;
}

function configureRadioLow(metaData: boolean): Record<string, string | number | string[]> {
  const icyBR = 64;
  const icyMetaint = 8192;
  const headers: Record<string, string | number | string[]> = {};
  headers['content-type'] = 'audio/mpeg';
  headers['icy-br'] = icyBR;
  headers['ice-audio-info'] = 'channels=2;samplerate=44100;bitrate=64';
  headers['icy-description'] = 'Radio Anonymous (Low Quality)';
  headers['icy-genre'] = 'radio';
  headers['icy-name'] = 'Radio Anonymous (Low Quality)';
  // noinspection HttpUrlsUsage
  headers['icy-url'] = 'http://anon.fm';
  if (metaData) {
    headers['server'] = 'Icecast 2.3.3-kh3';
    headers['icy-metaint'] = icyMetaint;
  }
  return headers;
}

function configureRadioAac(metaData: boolean): Record<string, string | number | string[]> {
  const icyBR = 12;
  const icyMetaint = 16000;
  const headers: Record<string, string | number | string[]> = {};
  headers['content-type'] = 'audio/aacp';
  headers['icy-br'] = icyBR;
  headers['ice-audio-info'] = 'SHOUT%5fAI;bitrate=12;SHOUT%5fAI%5fSAMPLERATE=32000';
  headers['icy-name'] = 'Anon.FM Bomjway Edition';
  headers['icy-url'] = 'htto://anon.fm';
  if (metaData) {
    headers['server'] = 'Icecast 2.3.3-kh3';
    headers['icy-metaint'] = icyMetaint;
  }
  return headers;
}

data.get(/\/streams\/radio(?:|.ogg|-low|.aac)/i, (req, res) => {
  let file = 'data/streams/radio';
  let metaData = false;
  if (req.headers['icy-metadata'] === '1') {
    metaData = true;
  }
  res.setHeader('server', 'Apache/2.0.63 (Unix) mod_ssl/2.0.63 OpenSSL/0.9.8e-fips-rhel5 FrontPage/5.0.2.2635');
  switch (req.url) {
    case '/streams/radio':
      setHeaders(res, configureRadio(metaData));
      break;
    case '/streams/radio.ogg':
      file += '-ogg';
      setHeaders(res, configureRadioOgg());
      break;
    case '/streams/radio-low':
      file += '-low';
      setHeaders(res, configureRadioLow(metaData));
      break;
    case '/streams/radio.aac':
      file += '-aac';
      setHeaders(res, configureRadioAac(metaData));
      break;
  }
  if (metaData) {
    file += '-metadata';
  }
  res.setHeader('icy-pub', 1);
  res.sendFile(resolve(file), {etag: false, cacheControl: false, acceptRanges: false, lastModified: false});
});
