/*
 * This file is part of AudioPlayer (https://gitlab.com/vitoyucepi/audio-player)
 * Copyright (C) 2023 Vitoyucepi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import {cleanWebpackPlugin, commonUserscriptConfig, cssLoaders, userscriptPlugin} from './common';
import path from 'path';
import {Configuration} from 'webpack';
import {merge} from 'webpack-merge';

const packageName = 'userscript-cdn';
const context = path.resolve(`../../@npm/audioplayer-userscript/`);

const cssRulesCdn = cssLoaders({useFontCdnSource: true}).cssRulesInline;

export const config: Configuration = merge(commonUserscriptConfig, {
  name: 'Userscript cdn configuration',
  context,
  output: {
    path: path.resolve(context, `dist/${packageName}`),
  },
  module: {
    rules: [cssRulesCdn],
  },
  plugins: [
    userscriptPlugin({
      headers: {
        name: 'Anon.fm audioplayer userscript. Cdn version',
        description: 'AudioPlayer for anon.fm. Userscript package. Fonts from cdn.',
      },
      baseUrlOptions: {suffix: packageName},
    }),
    cleanWebpackPlugin(),
  ],
});
