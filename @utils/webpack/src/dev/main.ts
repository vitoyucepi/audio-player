/*
 * This file is part of AudioPlayer (https://gitlab.com/vitoyucepi/audio-player)
 * Copyright (C) 2023 Vitoyucepi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import path from 'path';
import {cssRulesTest, fontRulesTest, tsRulesTest} from '../common';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import webpack, {Configuration, RuleSetRule} from 'webpack';
import {Configuration as DevServerConfiguration} from 'webpack-dev-server';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
import postcssPresetEnv from 'postcss-preset-env';
import postcssInputRange from 'postcss-input-range';

const useFontCdnSource = true;
const cssRules: RuleSetRule = {
  test: cssRulesTest,
  use: [
    'style-loader',
    {loader: 'css-loader', options: {sourceMap: true, importLoaders: 1, modules: {auto: true}}},
    {
      loader: 'postcss-loader',
      // eslint-disable-next-line @typescript-eslint/no-unsafe-call
      options: {sourceMap: true, postcssOptions: {plugins: [postcssInputRange(), postcssPresetEnv({stage: 0})]}},
    },
    {
      loader: 'sass-loader',
      options: {sourceMap: true, additionalData: `$font-source-cdn: ${useFontCdnSource.toString()};`, api: 'legacy'},
    },
  ],
};

const imageRules: RuleSetRule = {
  test: /\.(?:png|jpe?g|gif|svg)$/i,
  type: 'asset/resource',
};

const fontRules: RuleSetRule = {
  test: fontRulesTest,
  type: 'asset/resource',
};

const tsRules: RuleSetRule = {
  test: tsRulesTest,
  use: [{loader: 'ts-loader', options: {compilerOptions: {sourceMap: true}}}],
};

const GITPOD_HOT_RELOAD_PORT = 443;
const devServer: DevServerConfiguration = {
  host: '0.0.0.0',
  proxy: [
    {
      context: ['/data'],
      target: 'http://localhost:8081',
    },
  ],
  allowedHosts: 'all',
};

if (process.env.USE_GITPOD === 'true') {
  devServer.client = {webSocketURL: {port: GITPOD_HOT_RELOAD_PORT}};
}

const context = path.resolve('../../packages/audioplayer/');
const htmlWebpackPlugin = new HtmlWebpackPlugin({template: path.resolve(context, './src/index.html')});

const webpackDefinePlugin = new webpack.DefinePlugin({
  PRODUCTION: false,
});

export const config: Configuration = {
  context,
  module: {
    rules: [fontRules, imageRules, cssRules, tsRules],
  },
  devServer,
  mode: 'development',
  devtool: 'inline-source-map',
  plugins: [htmlWebpackPlugin, webpackDefinePlugin],
};
