<!--@formatter:off-->

# Audio player

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/c094b8f6e5404ed8ab4b189023e91816)](https://www.codacy.com/gl/vito-foss/audio-player/dashboard)
[![Userscript on npm](https://img.shields.io/npm/v/@vitoyucepi/audioplayer-userscript)](https://www.npmjs.com/package/@vitoyucepi/audioplayer-userscript)
[![Project license](https://img.shields.io/npm/l/@vitoyucepi/audioplayer-userscript)](https://www.gnu.org/licenses/agpl-3.0)
[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/vito-foss/audio-player)

<!--lint ignore no-shortcut-reference-link no-undefined-references-->

[TOC]

## About project

This project provides a drop-in replacement for flash player on [Anon.fm](https://anon.fm) site.

The player has to be able to:

- play music stream
- show tags
- change stream quality
- change music volume

## My vision of project

I think that player should:

- look like header line
- use modern technologies
- succeed ideas of [TWP contest player](https://gitlab.com/shkolopolis/twp-html5-player-contest)
- use userscript technology and can be deployed to server
- fit different desktop sizes

## Demo

The easiest way is to run [gitpod instance](#use-gitpod-online-ide-with-gitlab-integration).

## Install

### Before install

Available release versions. Choose one of them.

- **All-in-one userscript**
  - Large size, more than 220 kB
  - All resources built-in
  - Starts slower
- **Cdn userscript**
  - Smaller, less than 80 kB
  - Uses google font cdn
  - Starts faster

### Install browser addon

Download one of userscript addons for your browser.

- Tampermonkey
  - [Home page](https://www.tampermonkey.net/)
  - [Chrome](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo)
  - [Firefox](https://addons.mozilla.org/ru/firefox/addon/tampermonkey/)
  - [Microsoft Edge](https://microsoftedge.microsoft.com/addons/detail/iikmkjmpaadaobahmlepeloendndfphd)
  - [Opera](https://addons.opera.com/en/extensions/details/tampermonkey-beta/)
  - [Safari](https://apps.apple.com/us/app/tampermonkey/id1482490089)
- Violentmonkey
  - [Home page](https://violentmonkey.github.io/)
  - [Chrome](https://chrome.google.com/webstore/detail/violentmonkey/jinjaccalgkegednnccohejagnlnfdag)
  - [Firefox](https://addons.mozilla.org/ru/firefox/addon/violentmonkey/)
  - [Maxthon](http://extension.maxthon.com/detail/index.php?view_id=1680)
  - [Microsoft Edge](https://microsoftedge.microsoft.com/addons/detail/eeagobfjdenkkddmbclomhiblgggliao)
- Greasemonkey
  - [Home page](https://www.greasespot.net/)
  - [Firefox](https://addons.mozilla.org/ru/firefox/addon/greasemonkey/)
- Userscripts
  - [Home page](https://github.com/quoid/userscripts)
  - [Safari](https://apps.apple.com/us/app/userscripts/id1463298887)

### Install application

To install it you should:

1. Open link with one of the [releases](#before-install).
   - [All-in-one userscript](https://unpkg.com/@vitoyucepi/audioplayer-userscript@latest/dist/userscript/audioplayer.user.js)
   - [Cdn userscript](https://unpkg.com/@vitoyucepi/audioplayer-userscript@latest/dist/userscript-cdn/audioplayer.user.js)
1. See userscript addon special window with buttons install and cancel.
1. Press install.
1. Now everything is ready to use.\
   Open site [Anon.fm](https://anon.fm/), and you will see a new player at the top of the screen.

### After install

If you do not want to receive updates for script you should disable auto-update feature for userscript in your browser
addon.

## Development

### Build project by yourself

To build project by yourself you have to:

1. Install tools
   - `git`
   - `node`
   - `npm`
   - `yarn`
   - terminal or console
1. Get repository form gitlab by calling `git clone` or use your preferred text editor, ide, or git tool.\
   Choose one of two sources.
   - [https](https://gitlab.com/vito-foss/audio-player.git) with command
     `git clone https://gitlab.com/vito-foss/audio-player.git`
   - [ssh](git@gitlab.com:vito-foss/audio-player.git) with command `git clone git@gitlab.com:vito-foss/audio-player.git`
1. Install dependencies.\
   It will take about 1-5 minutes and about 700mb of free disk space.\
   `yarn install --pure-lockfile --non-interactive`
1. Build project. It will take about a minute.\
   `yarn build`

Now your own build versions is ready.\
Directories that contain release files:

- Deploy version: `./@npm/audioplayer-deploy/dist/`
- All-in-one userscript version: `./@npm/audioplayer-userscript/dist/userscript/`
- Cdn userscript version: `./@npm/audioplayer-userscript/dist/userscript-cdn/`

### Use gitpod online IDE with gitlab integration

1. Open project gitpod [page](https://gitpod.io/#https://gitlab.com/vito-foss/audio-player).
1. Login with your gitlab account.
1. Wait till your workspace is ready.
1. Your online IDE is ready to go. \
   By default, development environment will run.

## Project dependencies

1. Host and delivery

   - [Gitlab](https://gitlab.com/)
   - [Npm](https://www.npmjs.com/)
   - [Unpkg](https://unpkg.com/)
   - [Google fonts](https://fonts.google.com/)

1. Build and release

   - [Nodejs](https://nodejs.org/)
   - [Yarn](https://yarnpkg.com/)
   - [Lerna](https://lerna.js.org/)

1. Compile

   - [Webpack](https://webpack.js.org/)
   - [Typescript](https://www.typescriptlang.org/)

1. Frontend components

   - [Preact](https://preactjs.com/)
   - [React-icons](https://github.com/react-icons/react-icons)
   - [Storeon](https://github.com/storeon/storeon)
   - [Sass-mq](https://github.com/sass-mq/sass-mq)
   - [Neverthrow](https://github.com/supermacro/neverthrow)
   - [Openfonts](https://github.com/bedlaj/openfonts)
   - [Normalize.css](https://necolas.github.io/normalize.css/)

## License

```text
HTML5 Audio Player for anon.fm web site
Copyright (C) 2023 Vitoyucepi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```
