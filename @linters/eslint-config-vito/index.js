/*
 * This file is part of AudioPlayer (https://gitlab.com/vitoyucepi/audio-player)
 * Copyright (C) 2023 Vitoyucepi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = {
  rules: {
    quotes: [
      'error',
      'single',
      {
        avoidEscape: true,
      },
    ],
    'no-trailing-spaces': 'error',
    'space-before-blocks': 'error',
    'keyword-spacing': 'error',
    'space-before-function-paren': [
      'error',
      {
        anonymous: 'always',
        named: 'never',
        asyncArrow: 'always',
      },
    ],
    'space-in-parens': 'error',
    'no-whitespace-before-property': 'error',
    'block-spacing': 'error',
    'no-irregular-whitespace': 'error',
    'no-self-compare': 'error',
    'no-var': 'error',
    'comma-spacing': 'error',
    'array-bracket-spacing': 'error',
    'func-call-spacing': 'error',
    'key-spacing': 'error',
    'object-curly-spacing': 'error',
    'semi-spacing': 'error',
    semi: 'error',
    'semi-style': 'error',
    'space-unary-ops': 'error',
    'space-infix-ops': 'error',
    'brace-style': [
      'error',
      '1tbs',
      {
        allowSingleLine: true,
      },
    ],
    camelcase: 'error',
    'new-cap': 'error',
    'spaced-comment': [
      'error',
      'always',
      {
        block: {
          balanced: true,
        },
      },
    ],
    'unicode-bom': 'error',
    'no-new': 'error',
    'no-new-func': 'error',
    'no-new-wrappers': 'error',
    'padded-blocks': ['error', 'never'],
    'prefer-arrow-callback': 'error',
    'prefer-const': 'error',
    'no-else-return': 'error',
    'comma-dangle': [
      'error',
      {
        arrays: 'always-multiline',
        objects: 'always-multiline',
        imports: 'always-multiline',
        exports: 'always-multiline',
        functions: 'never',
      },
    ],
    'prefer-promise-reject-errors': 'error',
    'new-parens': 'error',
    'no-useless-return': 'error',
    'computed-property-spacing': 'error',
    eqeqeq: 'error',
    'no-dupe-keys': 'error',
    'no-useless-concat': 'error',
    'prefer-template': 'error',
    yoda: 'error',
    'require-await': 'error',
    'no-await-in-loop': 'error',
  },
};
