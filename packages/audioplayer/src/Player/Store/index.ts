/*
 * This file is part of AudioPlayer (https://gitlab.com/vitoyucepi/audio-player)
 * Copyright (C) 2023 Vitoyucepi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import {createStoreon} from 'storeon';
import {persistState} from '@storeon/localstorage';
import {crossTab} from '@storeon/crosstab';
import {
  metadataModule,
  PlayerEvents,
  PlayerState,
  playModule,
  qualityCrossTabFilter,
  qualityModule,
  qualityPersist,
  volumeCrossTabFilter,
  volumeModule,
  volumePersist,
} from './Modules';

const storeModules = [qualityModule, playModule, volumeModule, metadataModule];
const storePersist = persistState([...qualityPersist, ...volumePersist], {key: 'player'});
const storeCrossTab = crossTab({
  filter: (event) => volumeCrossTabFilter(event) || qualityCrossTabFilter(event),
  key: 'playerCT',
});

const store = createStoreon<PlayerState, PlayerEvents>([...storeModules, storePersist, storeCrossTab]);
export {store};
export * from './Modules';
