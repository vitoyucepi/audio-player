/*
 * This file is part of AudioPlayer (https://gitlab.com/vitoyucepi/audio-player)
 * Copyright (C) 2023 Vitoyucepi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import {StoreonModule} from 'storeon';
import {AUDIO_SOURCES} from '../../Consts';
import {CrossTabFilter, QualityEvents, QualityPersist, QualityState} from './types';

const qualityModule: StoreonModule<QualityState, QualityEvents> = (store) => {
  store.on('@init', () => {
    const currentSource = AUDIO_SOURCES.find((source) => source.default) || AUDIO_SOURCES[0];
    return {audioSources: AUDIO_SOURCES, currentSource};
  });
  store.on('quality/set', (state, value) => {
    const currentSource = state.audioSources.find((source) => source.name === value) || state.currentSource;
    return {currentSource};
  });
};

const qualityPersist: QualityPersist = ['currentSource'];

const qualityCrossTabFilter: CrossTabFilter = (event) => event === 'quality/set';

export {qualityCrossTabFilter, qualityPersist, qualityModule};
