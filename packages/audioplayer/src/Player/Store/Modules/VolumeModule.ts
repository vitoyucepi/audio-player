/*
 * This file is part of AudioPlayer (https://gitlab.com/vitoyucepi/audio-player)
 * Copyright (C) 2023 Vitoyucepi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import {StoreonModule} from 'storeon';
import {CrossTabFilter, VolumeEvents, VolumePersist, VolumeState} from './types';

const volumeModule: StoreonModule<VolumeState, VolumeEvents> = (store) => {
  store.on('@init', () => ({volume: 50}));
  store.on('volume/set', (_, value) => {
    if (value > 100) {
      return {volume: 100};
    }
    if (value < 0) {
      return {volume: 0};
    }
    return {volume: value};
  });
};

const volumePersist: VolumePersist = ['volume'];

const volumeCrossTabFilter: CrossTabFilter = (event) => event === 'volume/set';

export {volumeCrossTabFilter, volumePersist, volumeModule};
