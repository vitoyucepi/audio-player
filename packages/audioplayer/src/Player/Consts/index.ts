/*
 * This file is part of AudioPlayer (https://gitlab.com/vitoyucepi/audio-player)
 * Copyright (C) 2023 Vitoyucepi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/* global PRODUCTION */

interface AudioSource {
  source: string;
  bitrate: number;
  name: string;
  default?: boolean;
}

let STATE_TXT_SOURCE: string;
let MP3_SOURCE: string;
let AUDIO_SOURCES: AudioSource[];

if (PRODUCTION) {
  STATE_TXT_SOURCE = 'https://anon.fm/state.txt';
  MP3_SOURCE = 'https://anon.fm/streams/radio';
  // OGG_SOURCE = 'https://anon.fm/streams/radio.ogg';
  AUDIO_SOURCES = [
    {source: 'https://anon.fm/streams/radio', bitrate: 192, name: 'High', default: true},
    {source: 'https://anon.fm/streams/radio.ogg', bitrate: 112, name: 'Medium'},
    {source: 'https://anon.fm/streams/radio-low', bitrate: 64, name: 'Low'},
    {source: 'https://anon.fm/streams/radio.aac', bitrate: 12, name: 'UltraLow'},
  ];
} else {
  STATE_TXT_SOURCE = '/data/state.txt';
  MP3_SOURCE = '/data/streams/radio';
  // OGG_SOURCE = '/data/streams/radio.ogg';
  AUDIO_SOURCES = [
    {source: '/data/streams/radio', bitrate: 192, name: 'High', default: true},
    {source: '/data/streams/radio.ogg', bitrate: 112, name: 'Medium'},
    {source: '/data/streams/radio-low', bitrate: 64, name: 'Low'},
    {source: '/data/streams/radio.aac', bitrate: 12, name: 'UltraLow'},
  ];
}

const DEFAULT_TRACK_TITLE = 'Radio Anonymous';

export {DEFAULT_TRACK_TITLE, MP3_SOURCE, AUDIO_SOURCES, AudioSource, STATE_TXT_SOURCE};
