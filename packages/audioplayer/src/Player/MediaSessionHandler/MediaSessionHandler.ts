/*
 * This file is part of AudioPlayer (https://gitlab.com/vitoyucepi/audio-player)
 * Copyright (C) 2023 Vitoyucepi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import {DEFAULT_TRACK_TITLE} from '../Consts';
import {GenericTags} from '../Metadata';
import {MediaSessionState, MediaSessionStore} from './types';

export class MediaSessionHandler {
  private isInitialized = false;
  private isActionHandlersInit = false;

  private store: MediaSessionStore | undefined;

  // noinspection FunctionWithMoreThanThreeNegationsJS
  private static setTags(tags: GenericTags) {
    if (!MediaSessionHandler.isMediaSessionAvailable()) {
      return;
    }

    const {name, artist, title} = tags;
    const metaDataInit: MediaMetadataInit = {};
    if (title !== undefined) {
      metaDataInit.title = title;
      if (artist !== undefined) {
        metaDataInit.artist = artist;
      }
    } else if (name !== undefined) {
      metaDataInit.title = name;
    } else {
      metaDataInit.title = DEFAULT_TRACK_TITLE;
    }
    navigator.mediaSession.metadata = new MediaMetadata(metaDataInit);
  }

  private initActionHandlers() {
    if (this.isActionHandlersInit) {
      return;
    }
    this.isActionHandlersInit = true;
    const mediaSession = navigator.mediaSession;

    mediaSession.setActionHandler('play', () => {
      this.store?.dispatch('play/set', 'play');
    });
    mediaSession.setActionHandler('pause', () => {
      this.store?.dispatch('play/set', 'pause');
    });
    mediaSession.setActionHandler('stop', () => {
      this.store?.dispatch('play/set', 'stop');
    });
  }

  private onStoreChanged(state: MediaSessionState) {
    if (state.playStatus === 'play' || state.playStatus === 'pause') {
      this.initActionHandlers();
      MediaSessionHandler.setTags(state.tags);
    }
  }

  private static isMediaSessionAvailable() {
    if (navigator.mediaSession === undefined) {
      console.warn('MediaSession is not available.');
      return false;
    }
    return true;
  }

  init(store: MediaSessionStore): void {
    if (this.isInitialized) {
      return;
    }
    this.isInitialized = true;
    if (!MediaSessionHandler.isMediaSessionAvailable()) {
      return;
    }
    this.store = store;

    store.on('@changed', (state: MediaSessionState) => {
      this.onStoreChanged(state);
    });
  }
}
