/*
 * This file is part of AudioPlayer (https://gitlab.com/vitoyucepi/audio-player)
 * Copyright (C) 2023 Vitoyucepi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import {err, ok, Result} from 'neverthrow';
import {FailedToFetchError, findSequence, stringToUInt8Array, UtilsError} from '../../../Utils';
import {FailedToSplitBlock, MetadataError} from '../Errors';
import {IcyParserSplitResult} from './types';

class IcyParser {
  private static readonly HEADER_SEPARATOR = stringToUInt8Array('\r\n\r\n');
  private static readonly EXCLUDED_HTTP_HEADERS = ['ICY-PUB', 'ICY-AUDIO-INFO', 'ICY-BR', 'ICY-URL'];
  private static readonly ICY_STATUS_OK = 'ICY 200 OK';
  private static readonly METAINT_MULTIPLIER = 16;

  static split(uintArrayBlock: Uint8Array): Result<IcyParserSplitResult, MetadataError> {
    const headersOffset = findSequence(uintArrayBlock, this.HEADER_SEPARATOR);
    if (headersOffset === -1) {
      return err(new FailedToSplitBlock(`Cannot find split sequence '${this.HEADER_SEPARATOR.toString()}' in block`));
    }
    const headerBlock = uintArrayBlock.subarray(0, headersOffset);
    const bodyBlock = uintArrayBlock.subarray(headersOffset + 4);
    return ok({headerBlock, bodyBlock});
  }

  static parseHeader(headerBlock: Uint8Array): Result<Record<string, string>, UtilsError> {
    const decoder = new TextDecoder('utf-8');
    const headers = decoder.decode(headerBlock).split('\r\n');
    if (headers[0] !== this.ICY_STATUS_OK) {
      return err(new FailedToFetchError(`Icy status not ok. ${headers[0]}`));
    }
    headers.shift();
    const icyHttpHeaders: Record<string, string> = {};
    headers
      .map((header) => header.split(':'))
      .map(([key, value]) => [key.trim().toUpperCase(), value.trim()])
      .filter(([key]) => key.startsWith('ICY'))
      .filter(([key]) => !this.EXCLUDED_HTTP_HEADERS.includes(key))
      .forEach(([key, value]) => {
        icyHttpHeaders[key] = value;
      });
    return ok(icyHttpHeaders);
  }

  static parseHttpHeaders(headers: Headers): Result<Record<string, string>, Error> {
    const icyHttpHeaders: Record<string, string> = {};
    headers.forEach((value, key) => {
      const localKey = key.trim().toUpperCase();
      const localValue = value.trim();
      if (localKey.startsWith('ICY') && !this.EXCLUDED_HTTP_HEADERS.includes(localKey)) {
        icyHttpHeaders[localKey] = localValue;
      }
    });
    return ok(icyHttpHeaders);
  }

  static parseBody(bodyBlock: Uint8Array, metaInt: number): Result<Record<string, string>, MetadataError> {
    const metaSize = bodyBlock[metaInt] * this.METAINT_MULTIPLIER;
    const metaBlock = bodyBlock.subarray(metaInt + 1, metaInt + metaSize + 1);
    const decoder = new TextDecoder('utf-8');
    const headers = decoder.decode(metaBlock).split(';');
    headers.pop();
    const icyMetaHeaders: Record<string, string> = {};
    headers
      .filter((header) => header)
      .map((header) => header.split('='))
      .filter(([key, value]) => key && value)
      .map(([key, value]) => [key.trim().toUpperCase(), value.trim().slice(1, -1).trim()])
      .filter(([key, value]) => key.length > 0 && value.length > 0)
      .forEach(([key, value]) => {
        icyMetaHeaders[key] = value;
      });
    return ok(icyMetaHeaders);
  }
}

export {IcyParser};
