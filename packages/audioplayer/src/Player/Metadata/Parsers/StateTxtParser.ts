/*
 * This file is part of AudioPlayer (https://gitlab.com/vitoyucepi/audio-player)
 * Copyright (C) 2023 Vitoyucepi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import {ok, Result} from 'neverthrow';
import {MetadataError} from '../Errors';

class StateTxtParser {
  private static readonly FORBIDDEN_SYMBOLS = ['\ufffd'];
  private static readonly USELESS_KEYS = ['FILE', 'COVER', 'POS', 'ID', 'ISVIDEO'];

  static parseText(responseText: string): Result<Record<string, string>, MetadataError> {
    const txtArray = responseText.split('\n').filter((element) => element);
    const dataArray: [string, string][] = [];
    while (txtArray.length) {
      const [key, value] = txtArray.splice(-2);
      dataArray.push([key, value]);
    }
    const parsedData: Record<string, string> = {};
    dataArray
      .filter(([key]) => this.USELESS_KEYS.indexOf(key) === -1)
      .filter(([, value]) => !this.FORBIDDEN_SYMBOLS.some((symbol) => value.indexOf(symbol) > -1))
      .map(([key, value]) => [key.toUpperCase().trim(), value.trim()])
      .filter(([key, value]) => key.length > 0 && value.length > 0)
      .forEach(([key, value]) => {
        parsedData[key] = value;
      });
    return ok(parsedData);
  }
}

export {StateTxtParser};
