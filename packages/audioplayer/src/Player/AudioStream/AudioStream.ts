/*
 * This file is part of AudioPlayer (https://gitlab.com/vitoyucepi/audio-player)
 * Copyright (C) 2023 Vitoyucepi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import {AudioSource} from '../Consts';
import {AudioStreamState, AudioStreamStore} from './types';

class AudioStream {
  private static readonly STALLED_RETRY_TIMEOUT = 10 * 1000;
  // noinspection MagicNumberJS
  private static readonly STALLED_STOP_TIMEOUT = 30 * 1000;
  private isInitialized = false;
  private readonly stream = new Audio();
  private stalledRetryTimer: ReturnType<typeof setInterval> | undefined;
  private stalledStopTimer: ReturnType<typeof setTimeout> | undefined;

  constructor() {
    this.stream.preload = 'none';
  }

  init(store: AudioStreamStore): void {
    if (this.isInitialized) {
      return;
    }
    this.isInitialized = true;

    this.setSource(store.get().currentSource);
    this.setVolume(store.get().volume);
    this.setPlayStatus(store.get().playStatus);
    store.on('@changed', (state: AudioStreamState, data: AudioStreamState) => {
      this.onStoreChanged(state, data);
    });
    this.handleStreamEvents(store);
  }

  private onStoreChanged(state: AudioStreamState, data: AudioStreamState) {
    const {playStatus, volume, currentSource} = data;
    if (playStatus !== undefined) {
      this.setPlayStatus(playStatus);
    }
    if (volume !== undefined) {
      this.setVolume(volume);
    }
    if (currentSource !== undefined) {
      this.setSource(currentSource);
      this.setPlayStatus(state.playStatus);
    }
  }

  private handleStreamEvents(store: AudioStreamStore) {
    if (!this.isInitialized) {
      return;
    }

    this.stream.addEventListener('ended', () => {
      store.dispatch('play/set', 'stop');
    });

    this.stream.addEventListener('stalled', () => {
      this.handleStreamStalled(store);
    });

    this.stream.addEventListener('canplay', () => {
      this.resetRetryTimer();
      this.resetStopTimer();
    });
  }

  private handleStreamStalled(store: AudioStreamStore) {
    if (!this.isInitialized) {
      return;
    }

    if (this.stalledRetryTimer === undefined) {
      this.stalledRetryTimer = setInterval(() => {
        if (store.get().playStatus) {
          this.setPlayStatus('stop');
          this.setPlayStatus('play');
        } else {
          this.resetRetryTimer();
          this.resetStopTimer();
        }
      }, AudioStream.STALLED_RETRY_TIMEOUT);
    }
    if (this.stalledStopTimer === undefined) {
      this.stalledStopTimer = setTimeout(() => {
        store.dispatch('play/set', 'stop');
        this.resetRetryTimer();
      }, AudioStream.STALLED_STOP_TIMEOUT);
    }
  }

  private resetRetryTimer() {
    if (this.stalledRetryTimer === undefined) {
      return;
    }
    clearInterval(this.stalledRetryTimer);
    this.stalledRetryTimer = undefined;
  }

  private resetStopTimer() {
    if (this.stalledStopTimer === undefined) {
      return;
    }
    clearTimeout(this.stalledStopTimer);
    this.stalledStopTimer = undefined;
  }

  private setSource(source: AudioSource) {
    this.stream.src = source.source;
  }

  private static volumeTransformFunction(x: number) {
    return Math.pow(x, 2);
  }

  private setVolume(volume: number) {
    this.stream.volume = AudioStream.volumeTransformFunction(volume / 100);
  }

  private setPlayStatus(playStatus: AudioStreamState['playStatus']) {
    if (playStatus === 'play') {
      this.startStream();
    } else if (playStatus === 'pause') {
      this.pauseStream();
    } else {
      this.stopStream();
    }
  }

  private startStream() {
    if (this.stream.paused && this.stream.readyState === 4) {
      this.stopStream();
    }
    this.stream.play()?.catch(() => {
      // do nothing
    });
  }

  private pauseStream() {
    this.stream.pause();
    this.stream.currentTime = 0;
  }

  private stopStream() {
    const src = this.stream.src;
    this.stream.src = '';
    this.stream.src = src;
  }
}

export {AudioStream};
