/*
 * This file is part of AudioPlayer (https://gitlab.com/vitoyucepi/audio-player)
 * Copyright (C) 2023 Vitoyucepi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

interface FetchTimeoutErrorParameters {
  timeout: number;
}

class UtilsError extends Error {
  // noinspection LocalVariableNamingConventionJS, JSUnusedGlobalSymbols
  protected __brand: undefined;

  constructor(props = '') {
    super(props);
    this.name = 'UtilsError';
  }
}

class FailedToFetchError extends UtilsError {
  constructor(props = '') {
    super(`Failed to fetch. ${props}`.trim());
    this.name = 'FailedToFetchError';
  }
}

class FetchTimeoutError extends UtilsError {
  constructor(params: FetchTimeoutErrorParameters) {
    super(`Fetch timeout. Fetch did not finish in ${params.timeout} msecs`);
    this.name = 'FetchTimeoutError';
  }
}

export {UtilsError, FailedToFetchError, FetchTimeoutError};
