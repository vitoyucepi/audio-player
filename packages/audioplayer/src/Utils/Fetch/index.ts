/*
 * This file is part of AudioPlayer (https://gitlab.com/vitoyucepi/audio-player)
 * Copyright (C) 2023 Vitoyucepi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import {FailedToFetchError, FetchTimeoutError} from '../Errors';
import {err, ok, ResultAsync} from 'neverthrow';

interface CustomFetchOptions extends RequestInit {
  timeout?: number;
}

export function customFetch(
  source: Request | string,
  options: CustomFetchOptions = {},
): ResultAsync<Response, FailedToFetchError> {
  const {timeout = DEFAULT_TIMEOUT} = options;
  const controller = new AbortController();
  const fetchTimeout = setTimeout(() => controller.abort(), timeout);

  return ResultAsync.fromPromise(fetch(source, {signal: controller.signal, ...options}), (e) => {
    const error = <Error>e;
    if (error.name === 'AbortError') {
      return new FetchTimeoutError({timeout});
    }
    return new FailedToFetchError(error.message);
  }).andThen((fetchResponse) => {
    clearTimeout(fetchTimeout);
    if (fetchResponse.status !== STATUS_HTTP_OK || !fetchResponse.ok) {
      return err(new FailedToFetchError(fetchResponse.statusText));
    }
    return ok(fetchResponse);
  });
}

const STATUS_HTTP_OK = 200;
const DEFAULT_TIMEOUT = 10 * 1000;
