/*
 * This file is part of AudioPlayer (https://gitlab.com/vitoyucepi/audio-player)
 * Copyright (C) 2023 Vitoyucepi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import {FunctionComponent, h} from 'preact';
import './Styles/index.scss';
import {useStoreon} from 'storeon/preact';
import {DEFAULT_TRACK_TITLE, MetadataEvents, MetadataState} from '../../Player';

export const TrackTitleComponent: FunctionComponent = () => {
  const {tags} = useStoreon<MetadataState, MetadataEvents>('tags');
  let title;
  if (tags.title) {
    if (tags.artist) {
      title = `${tags.artist} - ${tags.title}`;
    } else if (tags.name) {
      title = `${tags.name} - ${tags.title}`;
    } else {
      title = tags.title;
    }
  } else if (tags.name) {
    title = tags.name;
  } else {
    title = DEFAULT_TRACK_TITLE;
  }
  return <div class="player-title">{title}</div>;
};
